# Код
## Управление запасами

### Классы:

1)	Кладовщик с методом (4) Результат проверки
```
public class Storekeeper
    {
        public void Results() 
        { 
            Console.WriteLine("[Кладовщик] 4.Результат проверки"); 
        }
    }
```
2) 	Система с методами: (1) Проверка наличия товара на складе и (3) Список товаров
```
public class System
    {
        ProductInStock productInStock = new ProductInStock();

        public void StockAvailability(System system)
        {
            Console.WriteLine("[Система] 1.Проверка наличия товара на складе");
            productInStock.StockAvailability(system);
        }
        public void ListOfProducts() 
        { 
            Console.WriteLine("[Система] 3.Список товаров");
            Storekeeper storekeeper = new Storekeeper();
            storekeeper.Results();
        }
    }
```
3)	Товар на складе с методом (2) Проверка наличия товара на складе
```
public class ProductInStock
    {
        
        public void StockAvailability(System system)
        {
            Console.WriteLine("[Товары на складе] 2. Проверка наличия товара на складе");
            system.ListOfProducts();
        }

    }
```
### Точка входа:
```
static void Main(string[] args)
        {
            System system = new System();
            Storekeeper storekeeper = new Storekeeper();

            system.StockAvailability(system);

            Console.ReadKey();
        }
```
Результат выполнения:
``` 
Управление запасами:
[Система] 1.Проверка наличия товара на складе
[Товары на складе] 2. Проверка наличия товара на складе
[Система] 3.Список товаров/Результат проверки
[Кладовщик] 4/8. Результат проверки
```
## Приемка товара

### Классы:
1)	Система с методами (1) Make Product Acceptance, (3) Сканирование штрихкода, (5) Определение места хранения
```
public class System
    {
        public int count;

        public void MakeProductAcceptance(System system, Storekeeper storekeeper, Warehouse warehouse)
        {
            Console.WriteLine("[Система] 1.Make Product Acceptance");

            Console.WriteLine("2. Создание 'прием товара'");
            AcceptanceOfGoods acceptanceOfGoods = new AcceptanceOfGoods();

            system.BarcodeScanning(system, storekeeper, warehouse);
        }

        public void BarcodeScanning(System system, Storekeeper storekeeper, Warehouse warehouse)
        {
            Console.WriteLine("Loop [для всех товаров]");
            for (int i = 0; i < count; i++)
            {
                Console.WriteLine($"{i+1}-ый товар");
                Console.WriteLine("[Система] 3. Сканирование штрихкода");
                productInStock.AddStockAvailability(system, storekeeper);
            }
            Console.WriteLine("End loop");
            
            
            warehouse.AddProductAcceptance();
            
        }

        public void DeterminationOfSrorageLocation(Storekeeper storekeeper)
        {
            Console.WriteLine("[Система] 5. Определение места хранения");
            Product product = new Product();
            product.AddProduct(storekeeper);
        }
        
    }
```
2)	Товары на складе с методом (4) Вносит сведения о категории товара
```
public class ProductInStock 
    {
        public void AddStockAvailability(System system, Storekeeper storekeeper) 
        {
            Console.WriteLine("[Товар на складе] 4. Вносит сведения о категории товара");
            system.DeterminationOfSrorageLocation(storekeeper);
        }

    }
```
3)	Товар с методом (6) Вносит товар в БД
```
public class Product 
    {
        Invoice invoice = new Invoice();
        public void AddProduct(Storekeeper storekeeper) 
        { 
            Console.WriteLine("[Товар] 6. Вносит товар в БД");
            invoice.CheckInvoice(storekeeper);
        }
    }
```
4)	Накладная с методом (7) Проверка соответствия с накладной
```
public class Invoice 
    {
        public void CheckInvoice(Storekeeper storekeeper) 
        {
            Console.WriteLine("[Накладная] 7. Проверка соотвествия с накладной");
            storekeeper.Results();
        }
    }
```
5)	Склад с методом (9) Add Product Acceptance
```
public class Warehouse
    {
        public void AddProductAcceptance() 
        {
            Console.WriteLine("[Склад] 9. AddProductAcceptance");
        }

    }
```
6)	Прием товара
```
public class AcceptanceOfGoods{}
```
7)	Кладовщик (см. управление запасами)


### Точка входа:
```
static void Main(string[] args)
        {
            System system = new System();
            Storekeeper storekeeper = new Storekeeper();
            Warehouse warehouse = new Warehouse();

            Console.Write("Количество товаров, принятых складом: ");
            system.count = Convert.ToInt32(Console.ReadLine());

            system.MakeProductAcceptance(system, storekeeper,warehouse);

            Console.ReadKey();
        }
```
Результат выполнения:
```
Приемка товаров:
Количество товаров, принятых складом: 3
[Система] 1.Make Product Acceptance
2. Создание 'прием товара'
Loop [для всех товаров]
1-ый товар
[Система] 3. Сканирование штрихкода
[Товар на складе] 4. Вносит сведения о категории товара
[Система] 5. Определение места хранения
[Товар] 6. Вносит товар в БД
[Накладная] 7. Проверка соотвествия с накладной
[Кладовщик] 4/8. Результат проверки
2-ый товар
[Система] 3. Сканирование штрихкода
[Товар на складе] 4. Вносит сведения о категории товара
[Система] 5. Определение места хранения
[Товар] 6. Вносит товар в БД
[Накладная] 7. Проверка соотвествия с накладной
[Кладовщик] 4/8. Результат проверки
3-ый товар
[Система] 3. Сканирование штрихкода
[Товар на складе] 4. Вносит сведения о категории товара
[Система] 5. Определение места хранения
[Товар] 6. Вносит товар в БД
[Накладная] 7. Проверка соотвествия с накладной
[Кладовщик] 4/8. Результат проверки
End loop
[Склад] 9. AddProductAcceptance
```
 
## Отправка товаров

### Классы:
1)	Система с методами (1) Make Send Product, (10) Make Payment, (3) Результат проверки
```
    public class System
    {
        public int count;

        ProductInStock productInStock = new ProductInStock();

        List<Order> ListOfOrders = new List<Order>(); //список заказов
        public int cost;

        public void MakeSendProduct(System system, List<Product> product, Warehouse warehouse) 
        {
            Console.WriteLine("[Система] 1. Make Send Product");
            productInStock.CheckAvailability(system);

            Console.WriteLine("[Отправка товара] 4. Создание 'отправка товара'");
            SendingGoods sendingGoods = new SendingGoods();


            Order order = new Order();
            order.CreateOrder(product);

            Console.WriteLine("[Список заказов] 6. Добавляется заказ");
            ListOfOrders.Add(order);//список закзов

            order.Price(product);

            system.MakePayment(sendingGoods,product,warehouse);
        }

        public void MakePayment(SendingGoods sendingGoods, List<Product> products, Warehouse warehouse) 
        {
            Console.WriteLine("[Система] 10. Make Payment");
            sendingGoods.MakePayment(cost, products);
            warehouse.PerfomanceOrder();
        }

        public void ListOfProduct()
        {
            Console.WriteLine("[Система] 3.Результат проверки");
        }
```
2)	Отправка товара с методом (11) MakePayment
```
public class SendingGoods 
    {
        public void MakePayment(int cost, List<Product> products) 
        {
            Console.WriteLine("[Отправка товара] 11. MakePayment");

            Console.WriteLine("12. Создание оплаты");
            Payment payment = new Payment();
            payment.CreatePayment(cost,products);
        }
    }
```
3)	Товар на складе с методом (2) Проверка наличия товара на складе
```
public class ProductInStock //товары на складе
    {

        public void CheckAvailability(System system)
        {
            Console.WriteLine("[Товары на складе] 2. Проверка наличия товара на складе");
            system.ListOfProduct();
        }

    }
```
4)	Заказ с методами (5) Создать заказ, (8) Price
```
public class Order
    {
        public int cost;

        public void CreateOrder(List<Product> product) 
        {
            Console.WriteLine("[Заказ] 5. Создать заказ");
        }
        public void Price(List<Product> products) 
        {
            Console.WriteLine("loop [для всех выбранных товаров]");
            int i = 1;
            foreach (Product product in products)
            {
                Console.WriteLine($"{i}-ый товар");
                product.GetPrice(product);
                Console.WriteLine("[Заказ] 8. Price");
                cost += product.price;
                i += 1;
            }
            Console.WriteLine("End loop");
            Console.WriteLine($"9. Общая стоимость заказа: {cost}");
        }   
    }
```
5)	Товар с методом (7) Get Price
```
public class Product 
    {
        public void GetPrice(Product product) 
        {
            Console.WriteLine("[Товар] 7. Get Price");
        }
    }
```
6)	Оплата с методом (13) Чек
```
public class Payment
    {
        public void CreatePayment(int cost, List<Product> products) 
        {
            Console.WriteLine("[Оплата] 13. Чек");
        }
    }
```
7)	Склад с методом (14) Направляет тех задание на сборку
```
public class Warehouse
    {
        
        public void PerfomanceOrder() 
        {
            Console.WriteLine("[Склад] Направляет тех задание на сборку");
        }

    }
```
### Точка входа:
```
static void Main(string[] args)
        {
            System system = new System();
            Warehouse warehouse = new Warehouse();
            List<Product> product = new List<Product>();

            Product product1 = new Product(1500,"ProductA");
            Product product2 = new Product(1780, "ProductB");

            product.Add(product1);
            product.Add(product2);

            Console.WriteLine("Отправка товаров");
            system.MakeSendProduct(system,product,warehouse);

            Console.ReadKey();
        }
```
Результат выполнения:
```
Отправка товаров
[Система] 1. Make Send Product
[Товары на складе] 2. Проверка наличия товара на складе
[Система] 3.Результат проверки
[Отправка товара] 4. Создание 'отправка товара'
[Заказ] 5. Создать заказ
[Список заказов] 6. Добавляется заказ
loop [для всех выбранных товаров]
1-ый товар
[Товар] 7. Get Price
[Заказ] 8. Price
2-ый товар
[Товар] 7. Get Price
[Заказ] 8. Price
End loop
9. Общая стоимость заказа: 3280
[Система] 10. Make Payment
[Отправка товара] 11. MakePayment
12. Создание оплаты
[Оплата] 13. Чек
[Склад] Направляет тех задание на сборку
```
 
## Приложение
```
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelirovanie
{

    public class System
    {
        public int count;
        //управление запасами
        ProductInStock productInStock = new ProductInStock();

        public void StockAvailability(System system)
        {
            Console.WriteLine("[Система] 1.Проверка наличия товара на складе");
            productInStock.StockAvailability(system);
        }
        public void ListOfProducts()
        {
            Console.WriteLine("[Система] 3.Список товаров/Результат проверки");
            Storekeeper storekeeper = new Storekeeper();
            storekeeper.Results();
        }



        //приемка товара:

        
        public void MakeProductAcceptance(System system, Storekeeper storekeeper, Warehouse warehouse)
        {
            Console.WriteLine("[Система] 1.Make Product Acceptance");

            Console.WriteLine("2. Создание 'прием товара'");
            AcceptanceOfGoods acceptanceOfGoods = new AcceptanceOfGoods();

            system.BarcodeScanning(system, storekeeper, warehouse);
        }
        public void BarcodeScanning(System system, Storekeeper storekeeper, Warehouse warehouse)
        {
            Console.WriteLine("Loop [для всех товаров]");
            for (int i = 0; i < count; i++)
            {
                Console.WriteLine($"{i+1}-ый товар");
                Console.WriteLine("[Система] 3. Сканирование штрихкода");
                productInStock.AddStockAvailability(system, storekeeper);
            }
            Console.WriteLine("End loop");
            
            
            warehouse.AddProductAcceptance();
            
        }
        public void DeterminationOfSrorageLocation(Storekeeper storekeeper)
        {
            Console.WriteLine("[Система] 5. Определение места хранения");
            Product product = new Product(2,"duj");
            product.AddProduct(storekeeper);
        }
        



        //отправка товара:

        List<Order> ListOfOrders = new List<Order>(); //список заказов
        public int cost;

        public void MakeSendProduct(System system, List<Product> product, Warehouse warehouse) 
        {
            Console.WriteLine("[Система] 1. Make Send Product");
            productInStock.CheckAvailability(system);

            Console.WriteLine("[Отправка товара] 4. Создание 'отправка товара'");
            SendingGoods sendingGoods = new SendingGoods();


            Order order = new Order();
            order.CreateOrder(product);

            Console.WriteLine("[Список заказов] 6. Добавляется заказ");
            ListOfOrders.Add(order);//список закзов

            order.Price(product);

            system.MakePayment(sendingGoods,product,warehouse);
        }

        public void MakePayment(SendingGoods sendingGoods, List<Product> products, Warehouse warehouse) 
        {
            Console.WriteLine("[Система] 10. Make Payment");
            sendingGoods.MakePayment(cost, products);
            warehouse.PerfomanceOrder();
        }

        public void ListOfProduct()
        {
            Console.WriteLine("[Система] 3.Результат проверки");
        }
        



        //точка входа:
        static void Main(string[] args)
        {
            System system = new System();
            Storekeeper storekeeper = new Storekeeper();
            Warehouse warehouse = new Warehouse();
            List<Product> product = new List<Product>();

            Product product1 = new Product(1500,"ProductA");
            Product product2 = new Product(1780, "ProductB");

            product.Add(product1);
            product.Add(product2);

            Console.WriteLine("Управление запасами:");
            system.StockAvailability(system);

            Console.WriteLine();

            Console.WriteLine("Приемка товаров:");
            Console.Write("Количество товаров, принятых складом: ");
            system.count = Convert.ToInt32(Console.ReadLine());
            system.MakeProductAcceptance(system, storekeeper,warehouse);

            Console.WriteLine();

            Console.WriteLine("Отправка товаров");
            system.MakeSendProduct(system,product,warehouse);

            Console.ReadKey();
        }

    }
    public class Storekeeper //кладовщик
    {
        public void Results()
        {
            Console.WriteLine("[Кладовщик] 4/8. Результат проверки");
        }
    }
    public class Product //товар
    {
        Invoice invoice = new Invoice();
        public int price;
        public string name;

        public Product(int price, string name)
        {
            this.price = price;
            this.name = name;
        }

        public void AddProduct(Storekeeper storekeeper) 
        { 
            Console.WriteLine("[Товар] 6. Вносит товар в БД");
            invoice.CheckInvoice(storekeeper);
        }

        public void GetPrice(Product product) 
        {
            Console.WriteLine("[Товар] 7. Get Price");
        }
    }
    public class ProductInStock //товары на складе
    {

        public void StockAvailability(System system)
        {
            Console.WriteLine("[Товары на складе] 2. Проверка наличия товара на складе");
            system.ListOfProducts();
        }

        public void CheckAvailability(System system)//отправка товара
        {
            Console.WriteLine("[Товары на складе] 2. Проверка наличия товара на складе");
            system.ListOfProduct();
        }


        public void AddStockAvailability(System system, Storekeeper storekeeper) 
        {
            Console.WriteLine("[Товар на складе] 4. Вносит сведения о категории товара");
            system.DeterminationOfSrorageLocation(storekeeper);
        }

    }
    public class Warehouse
    {
        public void AddProductAcceptance() 
        {
            Console.WriteLine("[Склад] 9. AddProductAcceptance");
        }
        public void PerfomanceOrder() 
        {
            Console.WriteLine("[Склад] Направляет тех задание на сборку");
        }

    }
    public class Invoice //накладная
    {
        public void CheckInvoice(Storekeeper storekeeper) 
        {
            Console.WriteLine("[Накладная] 7. Проверка соотвествия с накладной");
            storekeeper.Results();
        }
    }

    public class Order //заказ
    {
        public int cost;

        public void CreateOrder(List<Product> product) 
        {
            Console.WriteLine("[Заказ] 5. Создать заказ");
        }
        public void Price(List<Product> products) 
        {
            Console.WriteLine("loop [для всех выбранных товаров]");
            int i = 1;
            foreach (Product product in products)
            {
                Console.WriteLine($"{i}-ый товар");
                product.GetPrice(product);
                Console.WriteLine("[Заказ] 8. Price");
                cost += product.price;
                i += 1;
            }
            Console.WriteLine("End loop");
            Console.WriteLine($"9. Общая стоимость заказа: {cost}");
        }   
    }

    public class SendingGoods //отправка товара
    {
        public void MakePayment(int cost, List<Product> products) 
        {
            Console.WriteLine("[Отправка товара] 11. MakePayment");

            Console.WriteLine("12. Создание оплаты");
            Payment payment = new Payment();
            payment.CreatePayment(cost,products);
        }
    }

    public class AcceptanceOfGoods //прием товара
    { }

    public class Payment
    {
        public void CreatePayment(int cost, List<Product> products) 
        {
            Console.WriteLine("[Оплата] 13. Чек");
        }
    }

}
```
## Общий вывод:
```
Управление запасами:
[Система] 1.Проверка наличия товара на складе
[Товары на складе] 2. Проверка наличия товара на складе
[Система] 3.Список товаров/Результат проверки
[Кладовщик] 4/8. Результат проверки

Приемка товаров:
Количество товаров, принятых складом: 3
[Система] 1.Make Product Acceptance
2. Создание 'прием товара'
Loop [для всех товаров]
1-ый товар
[Система] 3. Сканирование штрихкода
[Товар на складе] 4. Вносит сведения о категории товара
[Система] 5. Определение места хранения
[Товар] 6. Вносит товар в БД
[Накладная] 7. Проверка соотвествия с накладной
[Кладовщик] 4/8. Результат проверки
2-ый товар
[Система] 3. Сканирование штрихкода
[Товар на складе] 4. Вносит сведения о категории товара
[Система] 5. Определение места хранения
[Товар] 6. Вносит товар в БД
[Накладная] 7. Проверка соотвествия с накладной
[Кладовщик] 4/8. Результат проверки
3-ый товар
[Система] 3. Сканирование штрихкода
[Товар на складе] 4. Вносит сведения о категории товара
[Система] 5. Определение места хранения
[Товар] 6. Вносит товар в БД
[Накладная] 7. Проверка соотвествия с накладной
[Кладовщик] 4/8. Результат проверки
End loop
[Склад] 9. AddProductAcceptance

Отправка товаров
[Система] 1. Make Send Product
[Товары на складе] 2. Проверка наличия товара на складе
[Система] 3.Результат проверки
[Отправка товара] 4. Создание 'отправка товара'
[Заказ] 5. Создать заказ
[Список заказов] 6. Добавляется заказ
loop [для всех выбранных товаров]
1-ый товар
[Товар] 7. Get Price
[Заказ] 8. Price
2-ый товар
[Товар] 7. Get Price
[Заказ] 8. Price
End loop
9. Общая стоимость заказа: 3280
[Система] 10. Make Payment
[Отправка товара] 11. MakePayment
12. Создание оплаты
[Оплата] 13. Чек
[Склад] Направляет тех задание на сборку
```